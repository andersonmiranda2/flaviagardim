<?php
/************
*
* Redirecionador de Retorno Autom�tico do PagSeguro para VirtueMart 2
* VirtueMart 2  (www.virtuemart.net)
* @version 1.1, Julho/2012
* @author Fernando Soares ( www.fernandosoares.com.br )
* @version $Id: npi_pagseg.php 09/07/2012 $
* @package Redirector
* @copyright Copyright (C) 2010-2012 Fernando Soares. All rights reserved.
*
*************/

$versao="";
$versao="1.1 Julho/2012";

if($_POST){
    $url = "";
	$req = "";
	$post_msg = "";
	$i = 1;
	if (isset ($_POST['notificationType'], $_POST['notificationCode'])) {
		//Defina a URL de notifica��o
		// *****************************************
		// Is the user using HTTPS?
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
		// Complete the URL
		$url .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
		$url = preg_replace('/(plugins\/vmpayment\/pagseguro)/', '', $url);
		$url = preg_replace('/(plugins\/vmpayment)/', '', $url); //no caso de Joomla! 1.5
		
		$url .= "index.php?option=com_virtuemart&view=pluginresponse&task=pluginnotification&tmpl=component";
    	// *****************************************

    	foreach ($_POST as $ipnkey => $ipnval) {
			$post_msg .= "chave " . $i++ . ": $ipnkey , valor: $ipnval\n";
		
			if (!get_magic_quotes_gpc()){
				$ipnval = addslashes($ipnval);
			}

			if($req != ""){
			    $req .= '&' . @$ipnkey . '=' . @$ipnval;
			}else{
			    $req .= @$ipnkey . '=' . @$ipnval;
			}
    	} // Notify string

		if ( !function_exists( 'curl_init' ) ) {
        	echo "FUN��O cURL DO PHP N�O EST� DISPON�VEL!!! VERIFIQUE COM SEU PROVEDOR DE HOSPEDAGEM.<br><br>";
			echo "DADOS RECEBIDOS:<br>";
			echo $post_msg;
			exit;
		}

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_HEADER, false);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 30);
		$fp = curl_exec ($ch);
		curl_close($ch);

		exit();
	} 
}else{

	echo '<html><head><title>Transa��o PagSeguro Conclu�da</title>';
	echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>';
	echo '<center><font size="3" face="arial,helvetica,sans-serif">';
	echo '<h2>Transa��o PagSeguro Conclu�da<br>___________________________________________</h2>';
	echo '<h3>Sua transa��o de pagamento foi conclu�da com sucesso!</h3>';
	echo '<span style="color:red"><h4>*** Aguarde esta transa��o SER APROVADA pelo PagSeguro. ***</h4></span>';
	echo 'Uma mensagem com os detalhes desta transa��o foi enviada para o seu e-mail.<br>';
	echo 'Voc� poder� acessar sua conta <a href="https://pagseguro.uol.com.br/?ind=623019">PagSeguro</a> para maiores detalhes.<br><br>';
	echo 'Voc� n�o tem uma conta no PagSeguro?<br><br>';
	echo 'Clique <a href="https://pagseguro.uol.com.br/?ind=623019">aqui</a> e crie uma j�. � gr�tis!<br>';
	echo '<!--Begin PagSeguro Logo --><A href="https://pagseguro.uol.com.br/?ind=623019" target=_blank><IMG  SRC=https://p.simg.uol.com.br/out/pagseguro/i/banners/pagamento/todos_estatico_550_100.gif border=0 alt="Abra sua conta PagSeguro e comece a aceitar cart�es de cr�dito e pagamentos online, imediatamente."></A><!-- End PagSeguro Logo --><br>';
	echo '</font><font size="2" face="arial,helvetica,sans-serif">Requer conta "Vendedor" ou "Empresarial" para o uso desta integra��o.<br><br>';
	echo '</font><font size="2" face="arial,helvetica,sans-serif">Redirecionador de Retorno Autom�tico PagSeguro<br>';
	if(date("Y") > 2010){
	    $ano = "-" . date("Y");
    }else{
		$ano = "";
	}
	echo 'Copyright 2010' . $ano . ' <a href="http://www.fernandosoares.com.br/">Fernando Soares</a><br>';
	echo '</font><font size="1" face="arial,helvetica,sans-serif">Vers�o '.$versao;
	echo '</font></center></html>';
}
?>